package com.example.geocaching;

import java.io.Serializable;
import java.util.ArrayList;

public class Quest implements Serializable {
    private int _questId;
    private String _questTitle;
    private String _questLocation;
    private ArrayList<String> _hints;

    public Quest(){

    }

    public Quest(int questId){ _questId = questId; }

    public Quest(int questId, String questTitle, String questLocation, ArrayList<String> hints){
        _questId = questId;
        _questTitle = questTitle;
        _questLocation = questLocation;
        _hints = hints;
    }

    public int getQuestId(){ return _questId; }

    public String getQuestTitle(){ return _questTitle; }
    public String getQuestLocation(){ return _questLocation; }
    public ArrayList<String> getHints(){ return _hints; }

    public String toString() {
        return _questTitle + " ("+ _questLocation+")";
    }
}
