package com.example.geocaching;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class HintAdapter extends BaseAdapter {
    Context _context;
    List<String> _hints;
    List<String> _visibleHints = new ArrayList<>();
    LayoutInflater _inflater;

    public HintAdapter(Context context, List<String> hints){
        _context = context;
        _hints = hints;
        _inflater = (LayoutInflater.from(_context));
    }
    @Override
    public int getCount() {
        return _hints.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = _inflater.inflate(R.layout.activity_hint_element,null);
        if(position == 0) convertView.setVisibility(View.VISIBLE);
        else if(_visibleHints.contains(_hints.get(position))) {
            convertView.setVisibility(View.VISIBLE);
        }
        else convertView.setVisibility(View.GONE);

        TextView hint = (TextView) convertView.findViewById(R.id.hintContent);
        Button button = (Button) convertView.findViewById(R.id.hintButton);
        hint.setText(_hints.get(position));
        if(position == _hints.size() - 1){
            button.setVisibility(View.GONE);
        }

        button.setOnClickListener(v -> {
            _visibleHints.add(_hints.get(position+1));
            this.notifyDataSetChanged(); //force le getView(pos + 1) et donc affiche l'indice suivant
        });
        return convertView;
    }
}
