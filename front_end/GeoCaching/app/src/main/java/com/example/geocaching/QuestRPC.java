package com.example.geocaching;


import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import org.apache.http.HttpRequest;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class QuestRPC extends AsyncTask<Integer, Integer, List<Quest>> {

    private volatile MainActivity screen;  //référence à l'écran

    static final String COOKIES_HEADER = "Set-Cookie";
    static CookieManager msCookieManager;

    private static final String POST_PARAMS = "{\"username\":\"mainuser\",\"password\":\"mainpass\"}";
    private String sessionId;

    public QuestRPC(MainActivity s) {
        this.screen = s ;
    }

    @Override
    protected List<Quest> doInBackground(Integer... integers) {
        msCookieManager = new CookieManager();
        CookieHandler.setDefault(msCookieManager);
        List<Quest> quests = new ArrayList<>();
        try {
            login();
            quests = getQuests();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return quests;
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected void onPostExecute(List<Quest> result) {
        //post-traitement de l'appel
        this.screen.populate(result); //callback
    }

    private void login() throws IOException {
        URL connectionURL = new URL("http://78.202.255.22:8080/login");
        HttpURLConnection urlConnection = (HttpURLConnection) connectionURL.openConnection();
        try {

            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json");

            // For POST only - START
            urlConnection.setDoOutput(true);
            OutputStream os = urlConnection.getOutputStream();
            os.write(POST_PARAMS.getBytes());
            os.flush();
            os.close();
            // For POST only - END

            int responseCode = urlConnection.getResponseCode();
            Log.v("RPC","POST Response Code :: " + responseCode);

            if (responseCode == HttpURLConnection.HTTP_OK) { //success
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        urlConnection.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                // print result
                Log.v("RPC",response.toString());
                Map<String, List<String>> headerFields = urlConnection.getHeaderFields();
                List<String> cookiesHeader = headerFields.get(COOKIES_HEADER);

                if (cookiesHeader != null) {
                    for (String cookie : cookiesHeader) {
                        msCookieManager.getCookieStore().add(null, HttpCookie.parse(cookie).get(0));
                        sessionId = HttpCookie.parse(cookie).get(0).toString();
                    }
                }
            } else {
                Log.v("RPC","POST request not worked");
            }
        } catch (IOException e) {
            Log.v("RPC",e.getMessage());
        } finally {
            urlConnection.disconnect();
        }
    }

    private List<Quest> getQuests() throws IOException {
        URL connectionURL = new URL("http://78.202.255.22:8080/quest");
        HttpURLConnection urlQuests = (HttpURLConnection) connectionURL.openConnection();
        List<Quest> quests = new ArrayList<>();
        try {
            urlQuests.setRequestMethod("GET");
            urlQuests.setRequestProperty("Content-Type", "application/json");

            Log.v("RPC","PHPSESSID="+sessionId);

            int responseCode = urlQuests.getResponseCode();
            Log.v("RPC","POST Response Code :: " + responseCode);

            if (responseCode == HttpURLConnection.HTTP_OK) { //success
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        urlQuests.getInputStream()));
                String inputLine;
                String response = "";

                while ((inputLine = in.readLine()) != null) {
                    response += inputLine;
                }
                in.close();
                JSONArray JSONresponse = new JSONArray(response);

                // print result
                for(int i = 0; i < JSONresponse.length(); i++){
                    JSONObject currentQuest = JSONresponse.getJSONObject(i);
                    ArrayList<String> hints = new ArrayList<>();
                    for(int j = 0; j < currentQuest.getJSONArray("clues").length(); j++){
                        String currentHint = currentQuest.getJSONArray("clues").getJSONObject(j).get("libelle").toString();
                        hints.add(currentHint);
                    }
                    quests.add(new Quest(Integer.parseInt(currentQuest.get("id").toString()),currentQuest.get("description").toString(), "Strasbourg",hints));
                }

            }
        } catch (ProtocolException | JSONException e) {
            e.printStackTrace();
        }finally {
            urlQuests.disconnect();
        }
        return quests;
    }
}