package com.example.geocaching;

import androidx.annotation.MainThread;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.ArrayList;
import java.util.List;

public class HintsActivity extends AppCompatActivity {
    private TextView _questTitle;
    private ListView _hintsList;
    private HintAdapter _hintsAdapter;
    private List<String> _hints;
    private Quest _selectedQuest;
    private Button _qrcodeButton;

    public static final String RAINBOW_ROOM = "room";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hints);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        _selectedQuest = (Quest) bundle.getSerializable(MainActivity.EXTRA_QUEST);
        _hints = _selectedQuest.getHints();

        _hintsList = findViewById(R.id.hintsList);
        _hintsAdapter = new HintAdapter(getApplicationContext(), _hints);
        _hintsList.setAdapter(_hintsAdapter);

        _questTitle = findViewById(R.id.questTitle);
        _questTitle.setText(_selectedQuest.getQuestTitle());

        _qrcodeButton = findViewById(R.id.qrcodeButton);
    }

    public void onClickQrCode(View view) {

            IntentIntegrator integrator = new IntentIntegrator(HintsActivity.this);
            integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
            integrator.setPrompt("Scan");
            integrator.setCameraId(0);
            integrator.setBeepEnabled(false);
            integrator.setBarcodeImageEnabled(false);
            integrator.initiateScan();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Intent intent = new Intent(HintsActivity.this, ChatActivity.class);

        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() == null) {
                Log.e("Scan*******", "Cancelled scan");

            } else {
                Log.e("Scan", "Scanned");
                intent.putExtra(RAINBOW_ROOM, result.getContents().toString());
                startActivity(intent);

            }
        } else {
            // This is important, otherwise the result will not be passed to the fragment
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}