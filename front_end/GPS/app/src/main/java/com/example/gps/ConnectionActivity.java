package com.example.gps;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.ale.listener.SigninResponseListener;
import com.ale.listener.StartResponseListener;
import com.ale.rainbowsdk.Connection;
import com.ale.rainbowsdk.RainbowSdk;

public class ConnectionActivity extends AppCompatActivity {

    private TextView m_textEmail;
    private TextView m_textPassword;
    private TextView m_connectButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection);
        initRainbowSDK();
        initViews();
    }

    private void initRainbowSDK(){
        RainbowSdk.instance().initialize(this, "4425bdb0a5e511ebbe33e9ed28980ec8", "Y7Q3TRKfbaM1ZQkn3gb5Uzdyh3etUgm9bLTPWYiLG8zXhL4Z3JDBrVNCLC4IKpTb");

        RainbowSdk.instance().connection().start(new StartResponseListener() {
            @Override
            public void onStartSucceeded() {
                // sign in with one of the following method
                Log.v("MainActivity", "start : success");
            }

            @Override
            public void onRequestFailed(RainbowSdk.ErrorCode errorCode, String err) {
                // Something was wrong
                Log.v("MainActivity", "start : fail " + errorCode + " " + err);
            }
        });
    }

    public void onConnectButton(View view) {
        RainbowSdk.instance().connection().signin(m_textEmail.getText().toString(), m_textPassword.getText().toString(), "sandbox.openrainbow.com", new SigninResponseListener() {
            @Override
            public void onSigninSucceeded() {
                // You are now connected to the production environment
                // Do something on the thread UI
                String roomId = "608ac3dbf2136d63a67e8071";
                runOnUiThread(() -> Toast.makeText(ConnectionActivity.this, "signin : success", Toast.LENGTH_LONG).show());
                Intent intent = new Intent(ConnectionActivity.this, MainActivity.class);
                startActivity(intent);
            }

            @Override
            public void onRequestFailed(RainbowSdk.ErrorCode errorCode, String err) {
                // Do something on the thread UI
                Log.v("MainActivity", "signin :fail " + errorCode + " " + err);
            }
        });
    }

    public void initViews(){
        m_textEmail = findViewById(R.id.textEmail);
        m_textPassword = findViewById(R.id.textPassword);
        m_connectButton = findViewById(R.id.connectButton);

        m_textEmail.setText("test.user@mycompany.com");
        m_textPassword.setText("Sup3rpwd!");
    }

    @Override
    protected void onStart() {
        super.onStart();

    }
}