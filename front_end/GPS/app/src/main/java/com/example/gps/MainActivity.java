package com.example.gps;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.LocationListener;
import android.os.Build;
import android.os.Bundle;
import android.os.Debug;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.ale.listener.SigninResponseListener;
import com.ale.listener.StartResponseListener;
import com.ale.rainbowsdk.RainbowSdk;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.time.Duration;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {


    private TextView m_lat;
    private TextView m_long;
    private Button m_bubbleButton;
    private String m_bubbleId;
    private String m_bubbleName;

    private Location m_location;

    // lists for permissions
    private ArrayList<String> m_permissionsToRequest;
    private ArrayList<String> m_permissionsRejected = new ArrayList<>();
    private ArrayList<String> m_permissions = new ArrayList<>();
    // integer for permissions results request
    private static final int ALL_PERMISSIONS_RESULT = 1011;

    private GoogleApiClient m_googleApiClient;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private LocationRequest m_locationRequest;
    private static final long UPDATE_INTERVAL = 5000, FASTEST_INTERVAL = 5000; // = 5 seconds

    public static final String RAINBOW_ROOM = "room";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initPermissions();
        initGoogleApi();
        initViews();
    }

    private void initPermissions(){
        // we add permissions we need to request location of the users
        m_permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        m_permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);

        m_permissionsToRequest = permissionsToRequest(m_permissions);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (m_permissionsToRequest.size() > 0) {
                requestPermissions(m_permissionsToRequest.
                        toArray(new String[m_permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
            }
        }
    }
    
    private ArrayList<String> permissionsToRequest(ArrayList<String> wantedPermissions) {
        ArrayList<String> result = new ArrayList<>();

        for (String perm : wantedPermissions) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case ALL_PERMISSIONS_RESULT:
                for (String perm : m_permissionsToRequest) {
                    if (!hasPermission(perm)) {
                        m_permissionsRejected.add(perm);
                    }
                }

                if (m_permissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(m_permissionsRejected.get(0))) {
                            new AlertDialog.Builder(MainActivity.this).
                                    setMessage("These permissions are mandatory to get your location. You need to allow them.").
                                    setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(m_permissionsRejected.
                                                        toArray(new String[m_permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    }).setNegativeButton("Cancel", null).create().show();

                            return;
                        }
                    }

                } else {
                    if (m_googleApiClient != null) {
                        m_googleApiClient.connect();
                    }
                }

                break;
        }
    }

    private void initGoogleApi(){
        m_googleApiClient = new GoogleApiClient.Builder(this).
                addApi(LocationServices.API).
                addConnectionCallbacks(this).
                addOnConnectionFailedListener(this).build();
    }

    private void initViews(){
        setContentView(R.layout.activity_main);

        m_lat = findViewById(R.id.tv_latitude);
        m_long = findViewById(R.id.tv_longitude);

        m_bubbleButton = findViewById(R.id.button_enter_bubble);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (m_googleApiClient != null) {
            m_googleApiClient.connect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!checkPlayServices()) {
            Log.d("ERROR","You need to install Google Play Services to use the App properly");
        }
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST);
            } else {
                finish();
            }

            return false;
        }

        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();

        // stop location updates
        if (m_googleApiClient != null  &&  m_googleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(m_googleApiClient, this);
            m_googleApiClient.disconnect();
        }
    }



    @Override
    public void onLocationChanged(@NonNull Location location) {
        m_location = location;
        updateCoordinates();
    }

    private void updateCoordinates() {
        m_long.setText("Longitude: " + m_location.getLongitude());
        m_lat.setText("Latitude: "+ m_location.getLatitude());
        checkBubble();
    }

    public void checkBubble(){
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://185.172.235.54:1087/zonesByRadius?latitude=" + m_location.getLatitude() +
                "&longitude=" + m_location.getLongitude();

        StringRequest stringRequest = new StringRequest(Request.Method.GET,url,
                response -> {
                    JSONObject bubble;
                    JSONArray bubbleArray;
                    try {
                        bubbleArray = new JSONArray(response);
                        bubble = bubbleArray.getJSONObject(0);
                        if(!m_bubbleId.equals(bubble.getString("key"))){
                            m_bubbleName = bubble.getString("name");
                            m_bubbleId = bubble.getString("key");
                            Toast.makeText(MainActivity.this,"Vous venez d'entrez dans la zone de la bulle "+ m_bubbleName, Toast.LENGTH_SHORT).show();
                        }
                        m_bubbleButton.setText("Rejoindre la bulle " + m_bubbleName);
                        m_bubbleButton.setVisibility(View.VISIBLE);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                },
                error -> m_lat.setText("ERROR HTTP: " + error));
        queue.add(stringRequest);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                &&  ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        // Permissions ok, we get last location
        m_location = LocationServices.FusedLocationApi.getLastLocation(m_googleApiClient);

        startLocationUpdates();
    }

    private void startLocationUpdates() {
        m_locationRequest = new LocationRequest();
        m_locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        m_locationRequest.setInterval(UPDATE_INTERVAL);
        m_locationRequest.setFastestInterval(FASTEST_INTERVAL);

        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                &&  ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "You need to enable permissions to display location !", Toast.LENGTH_SHORT).show();
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(m_googleApiClient, m_locationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {}

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {}

    public void enterBubble(View view) {
        Intent intent = new Intent(MainActivity.this, ChatActivity.class);
        intent.putExtra(RAINBOW_ROOM, m_bubbleId);
        startActivity(intent);
    }
}