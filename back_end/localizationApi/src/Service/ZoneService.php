<?php
namespace App\Service;


use App\Entity\Zone;
use Doctrine\ORM\EntityManagerInterface;

class ZoneService extends BaseService{

  public function __construct(EntityManagerInterface $entityManager)
  {
    parent::__construct($entityManager);
  }

  public function createZone($latitude, $longitude, $radius, $idBubble){
      $zone = new Zone();
      $zone->setLatitude($latitude);
      $zone->setLongitude($longitude);
      $zone->setRayon($radius);
      $zone->setDescription($description);
      $this->add($zone);
      return $zone;
  }
  
  public function getAllZones(){
      return $this->em->getRepository('App:Zone')->findAll();
  }

  public function findById($id){
      return $this->em->getRepository('App:Zone')->find($id);
  }

  public function updateZone($zone, $latitude, $longitude, $altitude, $radius, $idBubble){
      $zone->setLatitude($latitude);
      $zone->setLongitude($longitude);
      $zone->setRadius($radius);
      $zone->setIdBubble($idBubble);
      $this->update();
      return $zone;
  }

  //retourne la distance entre deux points en M
  public function vincentyGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
    {
      // convert from degrees to radians
      $latFrom = deg2rad($latitudeFrom);
      $lonFrom = deg2rad($longitudeFrom);
      $latTo = deg2rad($latitudeTo);
      $lonTo = deg2rad($longitudeTo);
    
      $lonDelta = $lonTo - $lonFrom;
      $a = pow(cos($latTo) * sin($lonDelta), 2) +
        pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
      $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);
    
      $angle = atan2(sqrt($a), $b);
      return $angle * $earthRadius;
    }
}