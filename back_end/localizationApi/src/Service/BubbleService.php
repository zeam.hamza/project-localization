<?php
namespace App\Service;


use App\Entity\Bubble;
use Doctrine\ORM\EntityManagerInterface;

class BubbleService extends BaseService{

  public function createBubble($name, $keycode){
      $bubble = new Bubble();
      $bubble->setName($name);
      $bubble->setKeycode($keycode);
      $this->add($bubble);
      return $bubble;
  }
  public function getAllBubbles(){
      return $this->em->getRepository('App:Bubble')->findAll();
  }
  public function findById($id){
      return $this->em->getRepository('App:Bubble')->find($id);
  }
  public function updateBubble($bubble, $nom, $keycode){
      $bubble->setName($nom);
      $bubble->setKeycode($keycode);
      $this->update();
      return $bubble;
  }
}