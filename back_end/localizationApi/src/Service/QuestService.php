<?php
namespace App\Service;


use App\Entity\Quest;
use Doctrine\ORM\EntityManagerInterface;

class QuestService extends BaseService{

    public function createQuest($latitude, $longitude, $altitude, $rayon, $description){
        $quest = new Quest();
        $quest->setLatitude($latitude);
        $quest->setLongitude($longitude);
        $quest->setAltitude($altitude);
        $quest->setRayon($rayon);
        $quest->setDescription($description);
        $this->add($quest);
        return $quest;
    }
    public function getAllQuests(){
        return $this->em->getRepository('App:Quest')->findAll();
    }
    public function findById($id){
        return $this->em->getRepository('App:Quest')->find($id);
    }
    public function updateQuest($quest, $latitude, $longitude, $altitude, $rayon, $description){
        $quest->setLatitude($latitude);
        $quest->setLongitude($longitude);
        $quest->setAltitude($altitude);
        $quest->setRayon($rayon);
        $quest->setDescription($description);
        $this->update();
        return $quest;
    }
    //retourne la distance entre deux points en M
    public function vincentyGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
      {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);
      
        $lonDelta = $lonTo - $lonFrom;
        $a = pow(cos($latTo) * sin($lonDelta), 2) +
          pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
        $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);
      
        $angle = atan2(sqrt($a), $b);
        return $angle * $earthRadius;
      }
}