<?php


namespace App\Service;

use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;

class ServiceSerialize
{
    public function serializeJson($data, $group, $circularLimit=1){
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $encoders = [new JsonEncoder()];
        $normalizer = [new DateTimeNormalizer(), new ObjectNormalizer($classMetadataFactory)];
        $serializer = new Serializer($normalizer, $encoders);

        return $serializer->serialize(
            $data,
            'json',
            [
                'circular_reference_limit' => $circularLimit,
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                },
                'groups' => $group
            ]
        );
    }
}