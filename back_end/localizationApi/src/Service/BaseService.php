<?php
namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;

abstract class BaseService
{
    /**
     * Entity Manager
     */
    protected $em;

    public function update()
    {
        $this->em->flush();
    }

    public function add($row)
    {
        $this->em->persist($row);
        $this->em->flush();
    }

    public function delete($row){
        $this->em->remove($row);
        $this->em->flush();
    }

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
}