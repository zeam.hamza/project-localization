<?php
namespace App\Service;


use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Clue;
use App\Service\QuestService;
use Doctrine\ORM\EntityManager;

class ClueService extends BaseService{

    private $serviceQuest;

    public function __construct(QuestService $serviceQuest, EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager);
        $this->serviceQuest = $serviceQuest;
    }
    public function createClue($libelle, $questId){
        $quest = $this->serviceQuest->findById($questId);
        if($quest == null){
            return false;
        }
        $clue = new Clue();
        $clue->setLibelle($libelle);
        $clue->setQuest($quest);
        $this->add($clue);
        return $clue;
    }
    public function getAllClue(){
        return $this->em->getRepository('App:Clue')->findAll();
    }
    public function updateClue($clue, $libelle, $questId){
        $clue->setLibelle($libelle);
        $clue->setQuest($this->serviceQuest->findById($questId));
        $this->update();
        return $clue;
    }
}