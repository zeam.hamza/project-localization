<?php

namespace App\Entity;

use App\Repository\ClueRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * @ORM\Entity(repositoryClass=ClueRepository::class)
 */
class Clue
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"questInfos","clueInfos"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"questInfos","clueInfos"})
     */
    private $libelle;

    /**
     * @ORM\ManyToOne(targetEntity=Quest::class, inversedBy="clues")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"clueInfos"})
     */
    private $quest;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getQuest(): ?Quest
    {
        return $this->quest;
    }

    public function setQuest(?Quest $quest): self
    {
        $this->quest = $quest;

        return $this;
    }
}
