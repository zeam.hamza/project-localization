<?php

namespace App\Entity;

use App\Repository\QuestRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * @ORM\Entity(repositoryClass=QuestRepository::class)
 */
class Quest
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"questInfos","clueInfos"})
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     * @Groups({"questInfos","clueInfos"})
     */
    private $latitude;

    /**
     * @ORM\Column(type="float")
     * @Groups({"questInfos","clueInfos"})
     */
    private $longitude;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"questInfos","clueInfos"})
     */
    private $altitude;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"questInfos","clueInfos"})
     */
    private $rayon;

    /**
     * @ORM\OneToMany(targetEntity=Clue::class, mappedBy="quest", orphanRemoval=true)
     * @Groups({"questInfos"})
     */
    private $clues;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"questInfos","clueInfos"})
     */
    private $description;

    public function __construct()
    {
        $this->clues = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getAltitude(): ?int
    {
        return $this->altitude;
    }

    public function setAltitude(int $altitude): self
    {
        $this->altitude = $altitude;

        return $this;
    }

    public function getRayon(): ?int
    {
        return $this->rayon;
    }

    public function setRayon(int $rayon): self
    {
        $this->rayon = $rayon;

        return $this;
    }

    /**
     * @return Collection|Clue[]
     */
    public function getClues(): Collection
    {
        return $this->clues;
    }

    public function addClue(Clue $clue): self
    {
        if (!$this->clues->contains($clue)) {
            $this->clues[] = $clue;
            $clue->setQuest($this);
        }

        return $this;
    }

    public function removeClue(Clue $clue): self
    {
        if ($this->clues->removeElement($clue)) {
            // set the owning side to null (unless already changed)
            if ($clue->getQuest() === $this) {
                $clue->setQuest(null);
            }
        }

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
