<?php

namespace App\Entity;

use App\Repository\BubbleRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BubbleRepository::class)
 */
class Bubble
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $keycode;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getKeycode(): ?string
    {
        return $this->keycode;
    }

    public function setKeycode(string $keycode): self
    {
        $this->keycode = $keycode;

        return $this;
    }
}
