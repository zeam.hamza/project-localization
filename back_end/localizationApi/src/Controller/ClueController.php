<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use App\Service\ClueService;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\ServiceSerialize;
use App\Entity\Clue;

class ClueController extends AbstractController{

    /**
     * @Route("/clue/add", methods={"POST"})
     */
    public function addClue(Request $request, ClueService $clueService, ServiceSerialize $serviceSerialize){
        $array = $request->toArray();
        $clue = $clueService->createClue($array['libelle'], $array['clueID']);
        if($clue == false){
            return new JsonResponse('Erreur dans les paramètres',400);
        }
        return JsonResponse::fromJsonString($serviceSerialize->serializeJson($clue, 'clueInfos'));
    }

    /**
     * @Route("/clue/", methods={"GET"})
     */
    public function getAllClues(Request $request, ClueService $clueService, ServiceSerialize $serviceSerialize){
        return JsonResponse::fromJsonString($serviceSerialize->serializeJson($clueService->getAllClue(), 'clueInfos'));
    }

    /**
     * @Route("/clue/details/{id}", methods={"GET"})
     */
    public function getClueDetails(Clue $clue, ServiceSerialize $serviceSerialize){
        return JsonResponse::fromJsonString($serviceSerialize->serializeJson($clue, "clueInfos"));
    }
    /**
     * @Route("/clue/remove/{id}", methods={"DELETE"})
     */
    public function removeClue(Clue $clue, ClueService $serviceClue){
        $serviceClue->delete($clue);
        return new JsonResponse('indice supprime');
    }

    /**
     * @Route("/clue/update/{id}", methods={"PATCH"})
     */
    public function updateClue(Clue $clue, ClueService $serviceClue, Request $request, ServiceSerialize $serviceSerialize){
        $array = $request->toArray();
        $clue = $serviceClue->updateClue($clue, $array['libelle'], $array['clueId']);
        return JsonResponse::fromJsonString($serviceSerialize->serializeJson($quest, 'clueInfos'));
    }

}