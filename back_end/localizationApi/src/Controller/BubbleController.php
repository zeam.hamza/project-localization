<?php

namespace App\Controller;

use App\Entity\Bubble;
use App\Service\BubbleService;
use App\Service\ServiceSerialize;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class BubbleController extends AbstractController
{
    /**
     * @Route("/bubble/add", methods={"POST"})
     */
    public function addBubble(Request $request, BubbleService $bubbleService, ServiceSerialize $serviceSerialize){
        $array = $request->toArray();
        $bubble = $bubbleService->createBubble($array['name'], $array['keycode']);
        if ($bubble == false){
            return new JsonResponse('Erreur dans les paramètres', 400);
        }
        return JsonResponse::fromJsonString($serviceSerialize->serializeJson($bubble, 'bubbleInfos'));
    }

    /**
     * @Route("/bubble/", methods={"GET"})
     */
    public function getAllBubbles(Request $request, BubbleService $bubbleService, ServiceSerialize $serviceSerialize){
        return JsonResponse::fromJsonString($serviceSerialize->serializeJson($bubbleService->getAllBubbles(), 'bubbleInfos'));
    }

    /**
     * @Route("/bubble/details/{id}", methods={"GET"})
     */
    public function getBubbleDetails(Bubble $bubble, ServiceSerialize $serviceSerialize){
        return JsonResponse::fromJsonString($serviceSerialize->serializeJson($bubble, "bubbleInfos"));
    }

    /**
     * @Route("/bubble/remove/{id}", methods={"DELETE"})
     */
    public function removeBubble(Bubble $bubble, BubbleService $bubbleService){
        $bubbleService->delete($bubble);
        return new JsonResponse('indice supprime');
    }

    /**
     * @Route("/bubble/update/{id}", methods={"PATCH"})
     */
    public function updateBubble(Bubble $bubble, BubbleService $bubbleService, Request $request, ServiceSerialize $serviceSerialize){
        $array = $request->toArray();
        $bubble = $serviceClue->updateBubble($bubble, $array['name'], $array['keycode']);
        return JsonResponse::fromJsonString($serviceSerialize->serializeJson($bubble, 'bubbleInfos'));
    }
}
