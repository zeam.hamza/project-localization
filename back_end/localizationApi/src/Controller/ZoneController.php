<?php

namespace App\Controller;

use App\Entity\Zone;
use App\Service\ZoneService;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

class ZoneController extends AbstractController
{
    #[Route('/zonesByRadius', name: 'zonesByRadius')]
    public function getAvailableZones(Request $request, ZoneService $service,EntityManagerInterface $em): Response
    {
        $clientLat = $request->query->get('latitude');
        $clientLon = $request->query->get('longitude');

        $response = array();
        $repository = $em->getRepository('App:Zone');
        $listZones = $repository->findAll();

        foreach($listZones as $zone) {
            if ($service->vincentyGreatCircleDistance($zone->getLatitude(), $zone->getLongitude(), $clientLat, $clientLon) <= $zone->getRadius()) {
                $zoneResponse = array('name'=>$zone->getBulle()->getName(),
                                    'key'=>$zone->getBulle()->getKeyCode());
                array_push($response, $zoneResponse);
            }
        }

        return new JsonResponse($response);
    }

    #[Route('/addZone', name: 'addZone')]
    public function addQuest(Request $request, ZoneService $service) : Response
    {
        $clientLat = $request->query->get('latitude');
        $clientLon = $request->query->get('longitude');
        $clientRad = $request->query->get('radius');
        $clientBub = $request->query->get('idBubble');

        $zone = $service->createZone($clientLat, $clientLon, $clientRad, $clientBub);
        return JsonResponse::fromJsonString($serviceSerialize->serializeJson($zone, 'zoneInfos'));
    }
}
