<?php

namespace App\Controller;

use App\Entity\Quest;
use App\Service\QuestService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\ServiceSerialize;
use Endroid\QrCodeBundle\Response\QrCodeResponse;
use Endroid\QrCode\Builder\BuilderInterface;


class QuestController extends AbstractController{
    /**
     * @Route("/quest/add", methods={"POST"})
     */
    public function addQuest(Request $request, QuestService $questService, ServiceSerialize $serviceSerialize){
        $array = $request->toArray();
        $quest = $questService->createQuest($array['latitude'], $array['longitude'], $array['altitude'], $array['rayon'], $array['description']);
        return JsonResponse::fromJsonString($serviceSerialize->serializeJson($quest, 'questInfos'));
    }

    /**
     * @Route("/quest/", methods={"GET"})
     */
    public function getAllQuests(Request $request, QuestService $questService, ServiceSerialize $serviceSerialize){
        return JsonResponse::fromJsonString($serviceSerialize->serializeJson($questService->getAllQuests(), 'questInfos'));
    }

    /**
     * @Route("/quest/details/{id}", methods={"GET"})
     */
    public function getQuestDetails(Quest $quest, ServiceSerialize $serviceSerialize){
        return JsonResponse::fromJsonString($serviceSerialize->serializeJson($quest, 'questInfos'));
    }
    /**
     * @Route("/quest/remove/{id}", methods={"DELETE"})
     */
    public function removeQuest(Quest $quest, QuestService $serviceQuest){
        $serviceQuest->delete($quest);
        return new JsonResponse('Quete supprime');
    }

    /**
     * @Route("/quest/update/{id}", methods={"PATCH"})
     */
    public function updateQuest(Quest $quest, QuestService $serviceQuest, Request $request, ServiceSerialize $serviceSerialize){
        $array = $request->toArray();
        $quest = $serviceQuest->updateQuest($quest, $array['latitude'], $array['longitude'], $array['altitude'], $array['rayon'], $array['description']);
        return JsonResponse::fromJsonString($serviceSerialize->serializeJson($quest, 'questInfos'));
    }

    /**
     * @Route("/quest/checkDistance/{id}", methods={"GET"})
     */
    public function checkDistance(Quest $quest, QuestService $serviceQuest, Request $request){
        $array = $request->toArray();
        $distance = $serviceQuest->vincentyGreatCircleDistance($array['latitude'], $array['longitude'],$quest->getLatitude(), $quest->getLongitude());
        if($distance <= $quest->getRayon()){
            return new JsonResponse(true);
        }else{
            return new JsonResponse(false);
        }
    }
    /**
     * @Route("/quest/qrCode/{id}", methods={"GET"})
     */
    public function getQrCode(Quest $quest, BuilderInterface $customQrCodeBuilder){
        $result = $customQrCodeBuilder
                    ->size(400)
                    ->margin(20)
                    ->data($quest->getId())
                    ->build();
        return new QrCodeResponse($result);
    }
    
}